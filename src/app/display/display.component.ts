import { Component, OnInit } from '@angular/core';
import { FetchApiService } from '../fetch-api.service';
import { Formatdata } from '../searchKey';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
})
export class DisplayComponent implements OnInit {
  value: string = '';
  results: Formatdata[] = [];
  title!: string;
  date: Date = new Date();
  constructor(private fetch: FetchApiService) {}

  ngOnInit(): void {}
  getValue(data: string) {
    this.value = data;
    console.log(this.value);
    this.fetch
      .getData(`https://hn.algolia.com/api/v1/search?query=${this.value}`)
      .subscribe((res) => {
        console.log(res);
        this.results = res.map((element: any) => {
          return {
            created_at:
              this.date.getFullYear() -
              new Date(element['created_at']).getFullYear(),
            title: element['title'],
            url: element['url'],
            author: element['author'],
            points: element['points'],
            num_comments: element['num_comments'],
            objectID: element['objectID'],
          };
        });
      });
  }
}
