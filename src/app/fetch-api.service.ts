import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class FetchApiService {
  constructor(private http: HttpClient) {}
  getData(key: string): Observable<any> {
    return this.http.get(key).pipe(
      map((response: any) => {
        return response.hits;
      })
    );
  }
}
