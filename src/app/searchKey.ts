export interface Formatdata {
  created_at: string;
  title: string;
  url: string;
  author: string;
  points: number;
  num_comments: number;
  objectID: number;
}
